/* you can import fonts across css with this rule */
module.exports = {
    test: /\.(woff|woff2|eot|ttf|otf)$/,
    use: [
        'file-loader',
    ]
};
