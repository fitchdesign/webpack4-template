module.exports = {
    css: require('./css.js'),
    fonts: require('./fonts.js'),
    images: require('./images.js'),
    jsx: require('./jsx.js'),
};
