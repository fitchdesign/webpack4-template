const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

module.exports = {
    dev: {
        test: /\.scss$/,
        use: [
            { loader: 'style-loader' },
            { loader: 'css-loader' },
            { loader: 'sass-loader' },
        ],
    },
    production: {
        test: /\.scss$/,
        use: ExtractTextWebpackPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'sass-loader'],
        }),
    },
};
