/* you can use `import './style.css'` with this rule */
module.exports = {
    test: /\.css$/,
    use: [
        'style-loader',
        'css-loader'
    ]
};
