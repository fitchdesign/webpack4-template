/* you can use `import 'image.png'` with this rule */
module.exports = {
    test: /\.(png|svg|jpg|gif)$/,
    use: [
        'file-loader'
    ]
};
