const plugins = require('./plugins');
const settings = require('./development.js');

// setting `mode` to production
settings.mode = 'production';

// removing source maps from production build
delete settings.devtool;

// delete devServer
delete settings.devServer;

// change output name to minified
settings.output.filename = 'js/[name].min.js';

// uglify build
settings.plugins.push(plugins.uglify);

module.exports = settings;
