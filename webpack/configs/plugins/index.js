module.exports = {
    clean: require('./clean'),
    copy: require('./copy'),
    core: require('./core'),
    hmp: require('./hmp'),
    html: require('./html'),
    nmp: require('./nmp'),
    uglify: require('./uglify'),
}
