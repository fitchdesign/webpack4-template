const path = require('path');

const { PATH_DIST } = require('../../webpack.config.js');

module.exports = new ExtractTextPlugin(path.join(PATH_DIST, 'css', 'style.css'));
