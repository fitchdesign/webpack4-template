/* imports */
const path = require('path');
const webpack = require('webpack');

/* variables */
const {
    PATH_DIST,
    PATH_SOURCE,
    VERSION,
} = require('../webpack.config');

/* other */
const rules = require('./rules');
const plugins = require('./plugins');

module.exports = {
    mode: 'development',

    entry: {
        app: path.join(PATH_SOURCE, 'index.js'),
    },

    output: {
        path: PATH_DIST,
        filename: 'js/[name].js',
        publicPath: '/',
    },

    devtool: 'eval',

    devServer: {
        contentBase: PATH_DIST,
        hot: true,
    },

    module: {
        rules: [
            rules.css,
        ],
    },

    plugins: [
        plugins.core,
        plugins.clean,
        plugins.html,
        plugins.copy,
        plugins.nmp,
        plugins.hmp,
    ],
}
