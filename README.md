Webpack 4 template
==

This template is done in a way that allows you to quickly create your own configs or quickly create new projects using the existing `development` and `production` configs.

Everything you need is in the `webpack/` folder. You can change the global settings used by webpack in the `webpack/webpack.config.js` file. In here you can change your source `(src/)` and distribution `(build/)` folders.

If you want to dig deeper into how the config are organised, have a look at the `webpack/configs/` folder. Inside you have several plugins and several rules added. Feel free to create a webpack config that fits your needs using the provided chunks for `rules` and `plugins`.

## NPM scripts
There are a couple of `npm scripts` added to the `package.json`. They should be enough to allow you to develop and build your application.

There's a few ways you can run your app in development, either through a `node.js` server or through a static server (PHP/Python, etc). It is totally up to you on how to manage this, however there's a few methods added for your convenience:

* `npm run dev:watch` should run the webpack with the --watch option on. [docs](https://webpack.js.org/guides/development/#using-watch-mode)

* `npm run dev:devserver` should run the webpack-dev-server which allows for HMR build-in. [docs](https://webpack.js.org/guides/development/#using-webpack-dev-server)

* `npm run dev:express` should run an express server and also add webpack's HRM support. [docs](https://webpack.js.org/guides/development/#using-webpack-dev-middleware)

* `npm run build` should run a production build, compiling everything and preparing your distribution.

* `npm start` is an alias to `npm run dev:devserver`.


## Development

### without `express()`

If you don't need a node.js server, just use the `npm start` or `npm run dev:devserver`. This will enable HMR by default.

### with `express()`
If you need a node.js server then run `npm run dev:express`. This will start the `server.dev.js` which handles the HMR for you.
